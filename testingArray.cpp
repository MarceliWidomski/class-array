/*
 * testingArray.cpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#include "gtest/gtest.h"
#include "Array.h"

class EmptyArrayOperationTestFixture: public testing::Test {
protected:
	virtual void SetUp() {
	}
	virtual void TearDown() {
	}
	Array testArray;
};
TEST_F(EmptyArrayOperationTestFixture, arrayConstructorCalledCorrectly) {
	EXPECT_EQ(0, testArray.getActualSize());
}
TEST_F(EmptyArrayOperationTestFixture, elementsAddedCorrectly) {
	testArray.addElementsAtTheEnd(5);
	EXPECT_EQ(0, testArray.getElement(0));
	EXPECT_EQ(0.4, testArray.getElement(4));
}
TEST_F(EmptyArrayOperationTestFixture, minGetCorrectly) {
	EXPECT_EQ(0, testArray.getMin());
}
TEST_F(EmptyArrayOperationTestFixture, maxGetCorrectly) {
	EXPECT_EQ(0, testArray.getMax());
}
TEST_F(EmptyArrayOperationTestFixture, elementOutOfRangeDeleted){
	EXPECT_NO_FATAL_FAILURE(testArray.deleteElement(5));
}
TEST_F(EmptyArrayOperationTestFixture, lastElementDeleted){
	EXPECT_NO_FATAL_FAILURE(testArray.deleteLastElement());
}
TEST_F (EmptyArrayOperationTestFixture, elementAtIndexAddedCorrectly){
	testArray.addElementAtIndex(0.1, 0);
	EXPECT_EQ(0.1, testArray.getElement(0));
	testArray.addElementAtIndex(0.4, 3);
	EXPECT_NE(0.4, testArray.getElement(4));
	EXPECT_EQ(1, testArray.getActualSize());
}
TEST_F (EmptyArrayOperationTestFixture, ptrAtArrayGotCorrectly){
	EXPECT_EQ(NULL, testArray.getArray());
}
TEST_F (EmptyArrayOperationTestFixture, elementFoundCorrectly){
	EXPECT_EQ(-1, testArray.findElement(0));
	EXPECT_EQ(-1, testArray.findElement(5));
}
TEST_F (EmptyArrayOperationTestFixture, sumOfElementsCalculatedCorrectly){
	EXPECT_EQ(0, testArray.sumOfElements());
}

class ArrayOperationTestFixture: public testing::Test {
protected:
	virtual void SetUp() {
		testArray.addElementsAtTheEnd(10);
	}
	virtual void TearDown() {
	}
	Array testArray; // it is only possible to invoke default constructor here
};

TEST_F(ArrayOperationTestFixture, arraySetCorrectly) {
	EXPECT_EQ(0, testArray.getElement(0));
	EXPECT_EQ(0.5, testArray.getElement(5));
	EXPECT_EQ(0.9, testArray.getElement(9));
}
TEST_F(ArrayOperationTestFixture, elementsAddedCorrectly) {
	testArray.addElementsAtTheEnd(5);
	EXPECT_EQ(1, testArray.getElement(10));
	EXPECT_EQ(1.4, testArray.getElement(14));
	EXPECT_NE(1.5, testArray.getElement(15));
}
TEST_F(ArrayOperationTestFixture, minGetCorrectly) {
	EXPECT_EQ(0, testArray.getMin());
	EXPECT_LT(testArray.getMin(), testArray.getElement(1));
	EXPECT_LT(testArray.getMin(), testArray.getElement(9));
}
TEST_F(ArrayOperationTestFixture, maxGetCorrectly) {
	EXPECT_EQ(0.9, testArray.getMax());
	EXPECT_GT(testArray.getMax(), testArray.getElement(1));
	EXPECT_GT(testArray.getMax(), testArray.getElement(8));
}
TEST_F(ArrayOperationTestFixture, elementsDeletedCorrectly){
	testArray.deleteElement(0);
	EXPECT_EQ(0.1, testArray.getElement(0));
	EXPECT_EQ(0.9, testArray.getElement(8));
	EXPECT_NE(0.9, testArray.getElement(9));
	testArray.deleteElement(0);
	EXPECT_EQ(0.2, testArray.getElement(0));
	EXPECT_EQ(0.9, testArray.getElement(7));
	EXPECT_NE(0.9, testArray.getElement(8));
	EXPECT_NO_FATAL_FAILURE(testArray.deleteElement(-1));
	EXPECT_NO_FATAL_FAILURE(testArray.deleteElement(testArray.getActualSize()));
}
TEST_F(ArrayOperationTestFixture, lastElementDeletedCorrectly){
	testArray.deleteLastElement();
	EXPECT_NE(0.9, testArray.getElement(9));
	EXPECT_EQ(0.8, testArray.getElement(8));
	EXPECT_EQ(9, testArray.getActualSize());
}
TEST_F (ArrayOperationTestFixture, elementAtIndexAddedCorrectly){
	testArray.addElementAtIndex(0.10, 3);
	EXPECT_EQ(0.2, testArray.getElement(2));
	EXPECT_EQ(0.10, testArray.getElement(3));
	EXPECT_EQ(0.3, testArray.getElement(4));
	EXPECT_EQ(11, testArray.getActualSize());
	EXPECT_EQ(0.9, testArray.getElement(10));
	EXPECT_FALSE(0.11 == testArray.getElement(12));
}
TEST_F (ArrayOperationTestFixture, elementFoundCorrectly){
	EXPECT_EQ(0, testArray.findElement(0));
	EXPECT_EQ(9, testArray.findElement(0.9));
	EXPECT_EQ(-1, testArray.findElement(-0.1));
	EXPECT_EQ(-1, testArray.findElement(1.0));
}
TEST_F (ArrayOperationTestFixture, ptrAtArrayGotCorrectly){
	EXPECT_EQ(0, *testArray.getArray());
}
TEST_F (ArrayOperationTestFixture, gettingElementByOperator){
	EXPECT_EQ(0, testArray[0]);
	EXPECT_EQ(0.9, testArray[9]);
	EXPECT_EQ(0.5, testArray[5]);
	EXPECT_EQ(0.9, testArray[10]);
}
TEST_F (ArrayOperationTestFixture, sumOfElementsCalculatedCorrectly){
	EXPECT_EQ(4.5, testArray.sumOfElements());
}
