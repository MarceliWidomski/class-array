/*
 * Array.h
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#ifndef ARRAY_H_
#define ARRAY_H_

const int maxSize(1000);

class Array {
public:
	Array();
	Array(int numberOfElements);
	~Array();
	double sumOfElements();
	void addElementsAtTheEnd(int numberOfElements);
	void addElementAtIndex (double elementValue, int elementIndex);
	void deleteElement(int elementIndex);
	void deleteLastElement();
	int findElement (double elementValue) const; // returns lowest index of element with expected value
	void printArray() const;
	double getMin() const;
	double getMax() const;
	double getElement(int elementIndex) const;
	int getActualSize() const {return actualSize;}
	const double* getArray() const;
	double operator[] (const int& index) const {return array[index];}

private:
	double array[maxSize];
	int actualSize;
};

#endif /* ARRAY_H_ */
