/*
 * Array.cpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#include "Array.h"
#include <iostream>

Array::Array() {
	actualSize = 0;
}
Array::Array(int numberOfElements) {
	actualSize = 0;
	addElementsAtTheEnd(numberOfElements);
}
Array::~Array() {
}
void Array::addElementsAtTheEnd(int numberOfElements) {
	double* ptrArrayElement = &array[actualSize];
	for (int i = 0; i < numberOfElements; ++i) {
		*(ptrArrayElement++) = (actualSize + i) / 10.0;
	}
	actualSize += numberOfElements;
}
void Array::printArray() const {
	std::cout << "Array [ ";
	for (int i = 0; i < actualSize; ++i)
		std::cout << array[i] << " ";
	std::cout << "]" << std::endl;
}
double Array::getMin() const {
	double min = array[0];
	for (int i = 1; i < actualSize; ++i) {
		if (min > array[i])
			min = array[i];
	}
	return min;
}
double Array::getMax() const {
	double max = array[0];
	for (int i = 1; i < actualSize; ++i) {
		if (max < array[i])
			max = array[i];
	}
	return max;
}
void Array::deleteElement(int elementIndex) {
	if (elementIndex >= 0 && elementIndex < actualSize && actualSize > 0) {
		for (int i = elementIndex; i < actualSize - 1; ++i) {
			array[i] = array[i + 1];
		}
		array[actualSize - 1] = 0;
		--actualSize;
	} else
		std::cout << "Invalid element index" << std::endl;
}
void Array::deleteLastElement() {
	if (actualSize > 0) {
		array[actualSize - 1] = 0;
		--actualSize;
	}
}
void Array::addElementAtIndex(double elementValue, int elementIndex) {
	if (actualSize < maxSize && elementIndex >= 0
			&& elementIndex <= actualSize) {
		for (int i = actualSize; i > elementIndex; --i) {
			array[i] = array[i - 1];
		}
		++actualSize;
		array[elementIndex] = elementValue;
	}
}
int Array::findElement(double elementValue) const {
	int i(0);
	for (i = 0; i < actualSize; ++i) {
		if (array[i] == elementValue) {
			return i;
		}
	}
	return -1;
}
double Array::sumOfElements() {
	double result(0);
	for (int i = 0; i < actualSize; ++i)
		result += array[i];
	return result;
}
double Array::getElement(int elementIndex) const {
	if (elementIndex >=0 && elementIndex < actualSize)
				return array[elementIndex];
}
const double* Array::getArray() const {
	if (!actualSize)
		return 0;
	else
		return array;
}
